#!/usr/bin/env bash

if [ "$1" == "all" ]; then
  docker-compose up -d --force-recreate --no-deps
else
  docker-compose up -d --force-recreate --no-deps "$@"
fi
