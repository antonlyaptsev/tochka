const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const utils = require('./utils')

passport.use(new LocalStrategy({
  usernameField: 'username',
  passwordField: 'username',
  passReqToCallback: true
}, (req, username, password, done) => {
  const user = {
    username: req.body.username
  }
  if (req.file) {
    user.avatar = req.file.path
    user.id = req.file.filename.split(':')[0]
  } else {
    user.id = utils.getUniqUUID()
    user.avatar = null
  }
  return done(null, user)
}))

passport.serializeUser((user, done) => {
  done(null, user)
})

passport.deserializeUser((id, done) => {
  done(null, id)
})

module.exports = passport
