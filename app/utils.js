const uuid = require('uuid/v4')
const clients = require('./clients')

function getUniqUUID () {
  let newUuid = uuid()
  if (clients[newUuid]) {
    newUuid = getUniqUUID()
  }
  return newUuid
}

function getSidCookie (cookies) {
  cookies = cookies.trim()
  const pairs = cookies.split(';').map(pair => pair.trim())
  const sidCookie = pairs.filter(cookie => cookie.indexOf('sid') > -1)[0]
  const sid = sidCookie.split('=')[1].split('.')[0].slice(4)
  return sid
}

function broadcastMessage (author_id, msg) {
  Object.values(clients)
        .filter(client => client.id !== author_id)
        .forEach(client => {
          if (client.ws && client.ws.readyState === 1) {
            client.ws.send(JSON.stringify(msg))
          }
        })
}

module.exports = {
  getUniqUUID,
  getSidCookie,
  broadcastMessage
}
