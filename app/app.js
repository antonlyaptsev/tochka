const path = require('path')
const bodyParser = require('body-parser')
const express = require('express')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
const multer = require('multer')
const uuid = require('uuid/v4')
const WebSocketServer = require('ws').Server
const Bluebird = require('bluebird')
const redis = Bluebird.promisifyAll(require('redis'))

const config = require('./config')
const passport = require('./passport')
const clients = require('./clients')
const utils = require('./utils')

const multerStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/avatars')
  },
  filename: function (req, file, cb) {
    const id = utils.getUniqUUID()
    cb(null, `${id}:${file.originalname}`)
  }
})
const upload = multer({ storage: multerStorage })

const socket = new WebSocketServer({ port: process.env.SOCKET_PORT })

const rdb = redis.createClient({
  host: config.redis.host,
  port: config.redis.port
})

const app = express()
app.disable('x-powered-by')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(session({
  name: 'sid',
  secret: config.session.secret,
  resave: false,
  saveUninitialized: false,
  store: new RedisStore({
    host: config.redis.host,
    port: config.redis.port
  })
}))
app.use(passport.initialize())
app.use(passport.session())

app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.json({
    error: err,
    message: err.message
  })
})

app.post('/login', upload.single('avatar'), (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    req.logIn(user, (err) => {
      if (err) return next(err)
      clients[user.id] = user
      return res.sendStatus(200)
    })
  })(req, res, next)
})

app.get('/logout', (req, res, next) => {
  delete clients[req.user.id]
  req.logout()
  res.sendStatus(200)
})

app.get('/session', (req, res, next) => {
  if (req.user) {
    if (!clients[req.user.id]) clients[req.user.id] = req.user
    return res.sendStatus(200)
  } else{
    return res.sendStatus(404)
  }
})

app.get('/session/get', (req, res, next) => {
  if (req.user) {
    const response = {
      user: req.user,
      users: Object.keys(clients).length ? Object.values(clients)
                   .filter(client => !!client && client.id !== req.user.id)
                   .map(client => {
                     return {
                       id: client.id,
                       username: client.username,
                       avatar: client.avatar
                     }
                   }) : []
    }
    return res.json(response)
  } else {
    return res.sendStatus(404)
  }
})

app.listen(process.env.PORT, () => {
  console.log('Listening on ' + process.env.PORT)
})

socket.on('connection', (ws, req) => {
  ws.on('message', async (message) => {
    message = JSON.parse(message)
    switch (message.type) {
      case 'connection':
        console.log('Connected user with id: ', message.user_id)
        if (!clients[message.user_id]) {
          const sid = utils.getSidCookie(req.headers.cookie)
          try {
            const session = await rdb.getAsync(`sess:${sid}`)
            const user = JSON.parse(session)['passport']['user']
            clients[message.user_id] = user
          } catch (e) {
            console.log(e)
          }
        }
        clients[message.user_id].ws = ws
        utils.broadcastMessage(message.user_id, {
          action: 'new_user',
          user: {
            id: clients[message.user_id].id,
            username: clients[message.user_id].username,
            avatar: clients[message.user_id].avatar
          }
        })
        break
      case 'message':
        utils.broadcastMessage(message.author.id, {
          action: 'message',
          author: message.author,
          text: message.text
        })
        break
      default:
        console.log(message)
    }
  })

  ws.on('close', async () => {
    const sid = utils.getSidCookie(req.headers.cookie)
    try {
      const session = await rdb.getAsync(`sess:${sid}`)
      const user = JSON.parse(session)['passport']['user']
      delete clients[user.id]
      utils.broadcastMessage(user.id, {
        action: 'close',
        user_id: user.id
      })
    } catch (e) {
      console.log(e)
    }
  })
})
