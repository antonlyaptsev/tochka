import axios from 'axios'
import Vue from 'vue'
import Router from 'vue-router'
import Signin from 'components/Signin/Index'
import Chat from 'components/Chat/Index'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'signin',
      component: Signin,
      beforeEnter: (to, from, next) => {
        axios.get('/api/session')
        .then(() => {
          next({ name: 'chat' })
        })
        .catch(() => {
          next()
        })
      }
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat,
      beforeEnter: (to, from, next) => {
        axios.get('/api/session')
        .then(() => {
          next()
        })
        .catch(() => {
          next({ name: 'signin' })
        })
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

export default router
