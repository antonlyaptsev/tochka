import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'

Vue.config.productionTip = false

Object.defineProperty(Vue.prototype, '$axios', {
  get () {
    return this.$root.axios
  }
})

Vue.filter('getAvatar', function (avatar) {
  return avatar ? `/public/avatars/${avatar}` : '/public/img/noavatar.png'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  data: {
    axios
  },
  components: { App }
})
