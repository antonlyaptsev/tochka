#!/usr/bin/env bash
source .env
echo "Installing project"

echo "Pulling and building docker images"
docker-compose build
docker-compose pull

echo "Installing node packages"
docker run --rm -ti --env-file .env -v "$PWD"/frontend:/src -w /src node:${NODE_VERSION} npm install
docker run --rm -ti --env-file .env -v "$PWD"/app:/src -w /src node:${NODE_VERSION} npm install

echo ""
echo "    +-------------------------------------------------------------------+"
echo "    | It seems that the installation was successful.                    |"
echo "    | Now run ./start.sh to start project and open http://localhost     |"
echo "    +-------------------------------------------------------------------+"
echo ""
