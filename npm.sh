#!/usr/bin/env bash
source .env
docker run --rm -ti -v "$PWD"/"$1":/src -w /src node:${NODE_VERSION} npm "${@:2}"
