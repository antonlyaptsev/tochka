#!/usr/bin/env bash

if [ "$1" == "all" ]; then
  docker-compose stop
else
  docker-compose stop "$@"
fi
